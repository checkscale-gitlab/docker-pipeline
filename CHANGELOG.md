# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## 0.3.0
2019-08-26

### Features

- more resilient pipeline on release stage (955644bf4d48bd9dec037cd05e9f0c59749a35f7)

## 0.2.0
2019-08-24

### Features

- change workflow for publish/release/deploy (1ff689a1a4c3427e97c5b07cab9e817f387b1ead)

## 0.1.1
2019-08-19

### Fixes

- CHANGELOG duplicate entries (0e9524dcefed9398f5095888d04746df33447f79)

## 0.1.0
2019-08-18

### Features

- initial commit (71b08be7645b06a5fa64baee802b28083e801ced)

### Fixes

- CHANGELOG commit (9be8889f566755e84ab118f4e38f810936f8190c)
